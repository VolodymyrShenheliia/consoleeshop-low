﻿using BLL.DTOs;
using BLL.Interfaces;

namespace BLL.Services.Interfaces
{
    public interface IUserService : IService<UserDto>
    {
        
    }
}