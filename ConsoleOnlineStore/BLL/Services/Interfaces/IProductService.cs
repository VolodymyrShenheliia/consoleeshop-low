﻿using BLL.DTOs;
using BLL.Interfaces;

namespace BLL.Services.Interfaces
{
    public interface IProductService : IService<ProductDto>
    {
        
    }
}