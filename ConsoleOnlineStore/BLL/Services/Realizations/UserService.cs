﻿using System.Collections.Generic;
using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.Infrastructure;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services.Realizations
{
    public class UserService : IUserService
    {
        private readonly IUoW _uow;
        private readonly IMapper _mapper;

        public UserService(IUoW uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }
        
        public IEnumerable<UserDto> GetAll()
        {
            var users = _uow.User.GetAll();

            return _mapper.Map<IEnumerable<UserDto>>(users);
        }

        public UserDto GetById(int id)
        {
            var user = _uow.User.GetById(id);
            
            if (user == null)
                throw new DbQueryResultNullException("Db query result to users is null");

            return _mapper.Map<UserDto>(user);
        }

        public UserDto Create(UserDto userDto)
        {
            if (userDto is null) throw new DbQueryResultNullException($"{nameof(userDto)} is null");
            
            DtoIdValidator<UserDto>.Validation(GetAll(), ref userDto);
            var user = _mapper.Map<User>(userDto);
            
            _uow.User.Create(user);
            
            return _mapper.Map<UserDto>(user);
        }

        public void Update(UserDto userDto)
        {
            var user =  _uow.User.GetById(userDto.Id);
            
            if (user == null)
                throw new DbQueryResultNullException("There isn't such user in db");
            
            user = _mapper.Map<User>(userDto);
            
            _uow.User.Update(user);
        }

        public void Remove(int id)
        {
            var user = _uow.User.GetById(id);
            
            if (user == null)
                throw new DbQueryResultNullException("No record to remove from users");

            _uow.User.Remove(user);
        }
    }
}