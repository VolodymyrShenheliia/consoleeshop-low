﻿using System.Collections.Generic;
using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.Infrastructure;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services.Realizations
{
    public class OrderService : IOrderService
    {
        private readonly IUoW _uow;
        private readonly IMapper _mapper;

        public OrderService(IUoW uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }
        public IEnumerable<OrderDto> GetAll()
        {
            var orders = _uow.Order.GetAll();

            return _mapper.Map<IEnumerable<OrderDto>>(orders);
        }

        public OrderDto GetById(int id)
        {
            var order = _uow.Order.GetById(id);
            
            if (order == null)
                throw new DbQueryResultNullException("Db query result to orders is null");

            return _mapper.Map<OrderDto>(order);
        }

        public OrderDto Create(OrderDto orderDto)
        {
            if (orderDto is null) throw new DbQueryResultNullException($"{nameof(orderDto)} is null");
            
            DtoIdValidator<OrderDto>.Validation(GetAll(), ref orderDto);
            var order = _mapper.Map<Order>(orderDto);
            
            _uow.Order.Create(order);
            
            return _mapper.Map<OrderDto>(orderDto);
        }

        public void Update(OrderDto orderDto)
        {
            var order =  _uow.Order.GetById(orderDto.Id);
            
            if (order == null)
                throw new DbQueryResultNullException("There isn't such order in db");
            
            order = _mapper.Map<Order>(orderDto);
            
            _uow.Order.Update(order);
        }

        public void Remove(int id)
        {
            var order = _uow.Order.GetById(id);
            
            if (order == null)
                throw new DbQueryResultNullException("No record to remove from orders");

            _uow.Order.Remove(order);
        }
    }
}