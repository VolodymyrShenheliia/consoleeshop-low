﻿using System.Collections.Generic;
using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.Infrastructure;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services.Realizations
{
    public class CategoryService : ICategoryService
    {
        private readonly IUoW _uow;
        private readonly IMapper _mapper;

        public CategoryService(IUoW uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }
        
        public IEnumerable<CategoryDto> GetAll()
        {
            var categories = _uow.Category.GetAll();

            return _mapper.Map<IEnumerable<CategoryDto>>(categories);
        }

        public CategoryDto GetById(int id)
        {
            var category = _uow.Category.GetById(id);
            
            if (category == null)
                throw new DbQueryResultNullException("Db query result to categories is null");

            return _mapper.Map<CategoryDto>(category);
        }

        public CategoryDto Create(CategoryDto categoryDto)
        {
            if (categoryDto is null) throw new DbQueryResultNullException($"{nameof(categoryDto)} is null");
            
            DtoIdValidator<CategoryDto>.Validation(GetAll(), ref categoryDto);
            var category = _mapper.Map<Category>(categoryDto);
            
            _uow.Category.Create(category);
            
            return _mapper.Map<CategoryDto>(category);
        }

        public void Update(CategoryDto categoryDto)
        {
            var category = _uow.Category.GetById(categoryDto.Id);
            
            if (category == null)
                throw new DbQueryResultNullException("There isn't such category in db");
            
            category = _mapper.Map<Category>(categoryDto);
            
            _uow.Category.Update(category);
        }

        public void Remove(int id)
        {
            var category = _uow.Category.GetById(id);
            
            if (category == null)
                throw new DbQueryResultNullException("No record to remove from categories");

            _uow.Category.Remove(category);
        }
    }
}