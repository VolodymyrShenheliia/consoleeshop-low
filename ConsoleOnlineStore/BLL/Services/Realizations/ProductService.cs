﻿using System.Collections.Generic;
using AutoMapper;
using BLL.DTOs;
using BLL.Exceptions;
using BLL.Infrastructure;
using BLL.Services.Interfaces;
using DAL.Entities;
using DAL.Interfaces;

namespace BLL.Services.Realizations
{
    public class ProductService : IProductService
    {
        private readonly IUoW _uow;
        private readonly IMapper _mapper;

        public ProductService(IUoW uow, IMapper mapper)
        {
            _uow = uow;
            _mapper = mapper;
        }
        
        public IEnumerable<ProductDto> GetAll()
        {
            var products = _uow.Product.GetAll();

            return _mapper.Map<IEnumerable<ProductDto>>(products);
        }

        public ProductDto GetById(int id)
        {
            var product = _uow.Product.GetById(id);
            
            if (product == null)
                throw new DbQueryResultNullException("Db query result to products is null");

            return _mapper.Map<ProductDto>(product);
        }

        public ProductDto Create(ProductDto productDto)
        {
            if (productDto is null) throw new DbQueryResultNullException($"{nameof(productDto)} is null");
            
            DtoIdValidator<ProductDto>.Validation(GetAll(), ref productDto);
            var product = _mapper.Map<Product>(productDto);
            
            _uow.Product.Create(product);
            
            return _mapper.Map<ProductDto>(product);
        }

        public void Update(ProductDto productDto)
        {
            var product = _uow.Product.GetById(productDto.Id);
            
            if (product == null)
                throw new DbQueryResultNullException("There isn't such product in db");
            
            product = _mapper.Map<Product>(productDto);
            
            _uow.Product.Update(product);
        }

        public void Remove(int id)
        {
            var product = _uow.Product.GetById(id);
            
            if (product == null)
                throw new DbQueryResultNullException("No record to remove from products");

            _uow.Product.Remove(product);
        }
    }
}