﻿using System.Collections.Generic;
using System.Linq;
using BLL.DTOs;

namespace BLL.Infrastructure
{
    public static class DtoIdValidator<TDto> where TDto : BaseDto
    {
        public static void Validation(IEnumerable<TDto> items, ref TDto dto)
        {
            var baseDto = dto;
            if (items?.Where(x => x.Id == baseDto.Id) == null) return;
            
            var newId = items?.Max(x => x.Id);
            dto.Id = (int) newId + 1;
        }
    }
}