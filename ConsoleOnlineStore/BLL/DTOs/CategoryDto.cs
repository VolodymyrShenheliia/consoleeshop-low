﻿namespace BLL.DTOs
{
    public class CategoryDto : BaseDto
    {
        public string Name { get; set; }

        public CategoryDto(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}