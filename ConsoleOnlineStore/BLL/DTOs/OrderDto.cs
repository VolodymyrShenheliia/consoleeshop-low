﻿using System.Collections.Generic;

namespace BLL.DTOs
{
    public class OrderDto : BaseDto
    {
        public OrderStatusDto Status { get; set; } = OrderStatusDto.New;
        public List<ProductDto> Products { get; set; }
        public UserDto User { get; set; }

        public OrderDto()
        {
            Products = new List<ProductDto>();
        }

        public OrderDto(int id, OrderStatusDto status, List<ProductDto> products, UserDto user)
        {
            Id = id;
            Status = status;
            Products = products;
            User = user;
        }

        public void AddProduct(ProductDto product, int amount)
        {
            Products.Add(new ProductDto(product.Id, product.Name, product.Category, product.Price, amount,
                product.Description));
        }
    }

    public enum OrderStatusDto
    {
        New,
        CanceledByUser, //set before received
        PaymentReceived,
        Sent,
        Received,
        Completed,
        CanceledByAdministrator
    }
}