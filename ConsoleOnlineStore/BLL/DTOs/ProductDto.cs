﻿namespace BLL.DTOs
{
    public class ProductDto : BaseDto
    {
        public string Name { get; set; }
        public CategoryDto Category { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }

        public ProductDto()
        {
            
        }
        
        public ProductDto(int id, string name, CategoryDto category, decimal price, int amount, string description)
        {
            Id = id;
            Name = name;
            Category = category;
            Price = price;
            Amount = amount;
            Description = description;
        }
    }
}