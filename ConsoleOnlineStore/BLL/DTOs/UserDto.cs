﻿namespace BLL.DTOs
{
    public class UserDto : BaseDto
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public UserRoleDto Role { get; set; }

        public UserDto()
        {
            
        }
       
        public UserDto(int id, string name, string surname, string address, int age, string email, string password, UserRoleDto role = UserRoleDto.None)
        {
            Id = id;
            Name = name;
            Surname = surname;
            Address = address;
            Age = age;
            Email = email;
            Password = password;
            Role = role;
        }
    }

    public enum UserRoleDto
    {
        None,
        User,
        Admin
    }
}