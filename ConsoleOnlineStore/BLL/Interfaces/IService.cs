﻿using System.Collections.Generic;
using BLL.DTOs;

namespace BLL.Interfaces
{
    public interface IService<TDto> where TDto : BaseDto
    {
        IEnumerable<TDto> GetAll(); 
        TDto GetById(int id);
        TDto Create(TDto dto);
        void Update(TDto dto);
        void Remove(int id);
    }
}