﻿using DAL.Repositories.Interfaces;

namespace DAL.Interfaces
{
    public interface IUoW
    {
        ICategoryRepository Category { get; }
        IProductRepository Product { get; }
        IOrderRepository Order { get; }
        IUserRepository User { get; }
    }
}