﻿using System.Collections.Generic;
using DAL.Entities;

namespace DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        TEntity GetById(int id); 
        IEnumerable<TEntity> GetAll();
        void Create(TEntity entity);
        void Remove(TEntity entity);
        void Update(TEntity entity);
    }
}