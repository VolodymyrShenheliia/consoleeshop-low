﻿using DAL.Interfaces;
using DAL.Repositories.Interfaces;

namespace DAL
{
    public class UoW : IUoW
    {
        public UoW(ICategoryRepository categoryRepository, IProductRepository productRepository, IOrderRepository orderRepository, IUserRepository userRepository)
        {
            Category = categoryRepository;
            Product = productRepository;
            Order = orderRepository;
            User = userRepository;
        }

        public ICategoryRepository Category { get; }
        public IProductRepository Product { get; }
        public IOrderRepository Order { get; }
        public IUserRepository User { get; }
    }
}