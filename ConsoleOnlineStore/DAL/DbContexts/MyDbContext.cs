﻿using System.Collections.Generic;
using DAL.Entities;

namespace DAL.DbContexts
{
    public static class MyDbContext
    {
        public static List<User> Users { get; }
        public static List<Product> Products { get; }
        public static List<Category> Categories { get; }
        public static List<Order> Orders { get; }

        static MyDbContext()
        {
            Users = new List<User>()
            {
                new User { Id = 1, Name = "Vladimir", Surname = "Shengeliya", Age = 20, Email = "vladimir231200@gmail.com", Password = "123456", Role = UserRole.Admin },
                new User { Id = 2, Name = "Polina", Surname = "Bykova", Age = 21, Email = "pbykova17@gmail.com", Password = "123456", Role = UserRole.User }
            };

            Categories = new List<Category>()
            {
                new Category(1, "Laptops"),
                new Category(2, "Desktop"),
                new Category(3, "Monitor"),
                new Category(4, "Mobile")
            };
            
            Products = new List<Product>()
            {
                new Product { Id = 1, Name = "Dell 15\"", Amount = 10, Category = Categories[0], Description = "aaaa", Price = 999.99m },
                new Product { Id = 2, Name = "Dell 17\"", Amount = 14, Category = Categories[0], Description = "bbbb", Price = 1099.99m },
                new Product { Id = 3, Name = "Macbook Pro 13\"", Amount = 16, Category = Categories[0], Description = "cccc", Price = 1599.99m },
                new Product { Id = 4, Name = "Macbook Pro 16\"", Amount = 5, Category = Categories[0], Description = "dddd", Price = 1999.99m },
                new Product { Id = 5, Name = "Macbook Air", Amount = 8, Category = Categories[0], Description = "eeee", Price = 999.99m }
            };

            Orders = new List<Order>();
        }
    }
}