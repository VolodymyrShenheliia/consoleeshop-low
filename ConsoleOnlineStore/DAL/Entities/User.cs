﻿namespace DAL.Entities
{
    public class User : BaseEntity
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public int Age { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public UserRole Role { get; set; }

        public User()
        {
            
        }
       
        public User(int id, string name, string surname, string address, int age, string email, string password, UserRole role = UserRole.None)
        {
            Id = id;
            Name = name;
            Surname = surname;
            Address = address;
            Age = age;
            Email = email;
            Password = password;
            Role = role;
        }
    }

    public enum UserRole
    {
        None,
        User,
        Admin
    }
}