﻿using System.Collections.Generic;

namespace DAL.Entities
{
    public class Order : BaseEntity
    {
        public OrderStatus Status { get; set; } = OrderStatus.New;
        public List<Product> Products { get; set; }
        public User User { get; set; }

        public Order()
        {
            Products = new List<Product>();
        }

        public Order(int id, OrderStatus status, List<Product> products, User user)
        {
            Id = id;
            Status = status;
            Products = products;
            User = user;
        }

        public void AddProduct(Product product, int amount)
        {
            Products.Add(new Product(product.Id, product.Name, product.Category, product.Price, amount, product.Description));
        }
    }

    public enum OrderStatus
    {
        New,
        CanceledByUser, //set before received
        PaymentReceived,
        Sent,
        Received,
        Completed,
        CanceledByAdministrator
    }
}