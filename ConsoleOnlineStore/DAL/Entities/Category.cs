﻿namespace DAL.Entities
{
    public class Category : BaseEntity
    {
        public string Name { get; set; }

        public Category(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}