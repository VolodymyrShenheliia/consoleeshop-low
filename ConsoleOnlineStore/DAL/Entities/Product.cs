﻿namespace DAL.Entities
{
    public class Product : BaseEntity
    {
        public string Name { get; set; }
        public Category Category { get; set; }
        public decimal Price { get; set; }
        public int Amount { get; set; }
        public string Description { get; set; }

        public Product()
        {
            
        }
        
        public Product(int id, string name, Category category, decimal price, int amount, string description)
        {
            Id = id;
            Name = name;
            Category = category;
            Price = price;
            Amount = amount;
            Description = description;
        }
    }
}