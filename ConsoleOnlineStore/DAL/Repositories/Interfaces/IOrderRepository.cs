﻿using DAL.Entities;
using DAL.Interfaces;

namespace DAL.Repositories.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        
    }
}