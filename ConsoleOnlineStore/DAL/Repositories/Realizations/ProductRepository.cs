﻿using System.Collections.Generic;
using System.Linq;
using DAL.DbContexts;
using DAL.Entities;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories.Realizations
{
    public class ProductRepository : IProductRepository
    {
        public Product GetById(int id)
        {
            Product product = null;
            var prodFromStaticDb = MyDbContext.Products.Find(x => x.Id == id);
            if (prodFromStaticDb != null)
            {
                product = new Product(prodFromStaticDb.Id, prodFromStaticDb.Name, prodFromStaticDb.Category, prodFromStaticDb.Price,
                    prodFromStaticDb.Amount, prodFromStaticDb.Description);
            }

            return product;
        }

        public IEnumerable<Product> GetAll() => MyDbContext.Products.ToArray();

        public void Create(Product entity) => MyDbContext.Products.Add(entity);

        public void Remove(Product entity)
        { 
            var entityToRemove = MyDbContext.Products.FirstOrDefault(x => x.Id == entity.Id);
            MyDbContext.Products.Remove(entityToRemove);
        } 

        public void Update(Product entity)
        {
            Remove(entity);
            Create(entity);
        }
    }
}