﻿using System.Collections.Generic;
using System.Linq;
using DAL.DbContexts;
using DAL.Entities;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories.Realizations
{
    public class CategoryRepository : ICategoryRepository
    {
        public Category GetById(int id)
        {
            Category category = null;
            
            var categoryFromStaticDb = MyDbContext.Categories.Find(x => x.Id == id);
            if (categoryFromStaticDb != null)
                category = new Category(categoryFromStaticDb.Id, categoryFromStaticDb.Name);

            return category;
        }

        public IEnumerable<Category> GetAll() => MyDbContext.Categories.ToArray();

        public void Create(Category entity) => MyDbContext.Categories.Add(entity);

        public void Remove(Category entity)
        { 
            var entityToRemove = MyDbContext.Categories.FirstOrDefault(x => x.Id == entity.Id);
            MyDbContext.Categories.Remove(entityToRemove);
        } 

        public void Update(Category entity)
        {
            Remove(entity);
            Create(entity);
        }
    }
}