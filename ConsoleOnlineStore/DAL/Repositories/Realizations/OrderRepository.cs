﻿using System.Collections.Generic;
using System.Linq;
using DAL.DbContexts;
using DAL.Entities;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories.Realizations
{
    public class OrderRepository : IOrderRepository
    {
        public Order GetById(int id)
        {
            Order order = null;
            
            var orderFromStaticDb = MyDbContext.Orders.Find(x => x.Id == id);
            if (orderFromStaticDb != null)
                order = new Order(orderFromStaticDb.Id, orderFromStaticDb.Status, orderFromStaticDb.Products.ToList(), orderFromStaticDb.User);

            return order;
        }

        public IEnumerable<Order> GetAll()  => MyDbContext.Orders.ToArray();

        public void Create(Order entity) => MyDbContext.Orders.Add(entity);

        public void Remove(Order entity)
        { 
            var entityToRemove = MyDbContext.Orders.FirstOrDefault(x => x.Id == entity.Id);
            MyDbContext.Orders.Remove(entityToRemove);
        } 

        public void Update(Order entity)
        {
            Remove(entity);
            Create(entity);
        }
    }
}