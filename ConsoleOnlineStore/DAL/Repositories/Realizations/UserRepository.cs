﻿using System.Collections.Generic;
using System.Linq;
using DAL.DbContexts;
using DAL.Entities;
using DAL.Repositories.Interfaces;

namespace DAL.Repositories.Realizations
{
    public class UserRepository : IUserRepository
    {
        public User GetById(int id)
        {
            User user = null;
            var userFromStaticDb = MyDbContext.Users.Find(x => x.Id == id);
            if (userFromStaticDb != null)
            {
                user = new User(userFromStaticDb.Id, userFromStaticDb.Name, userFromStaticDb.Surname, userFromStaticDb.Address, userFromStaticDb.Age,
                    userFromStaticDb.Email, userFromStaticDb.Password, userFromStaticDb.Role);
            }

            return user;
        }

        public IEnumerable<User> GetAll() => MyDbContext.Users.ToArray();

        public void Create(User entity) => MyDbContext.Users.Add(entity);

        public void Remove(User entity)
        { 
            var entityToRemove = MyDbContext.Users.FirstOrDefault(x => x.Id == entity.Id);
            MyDbContext.Users.Remove(entityToRemove);
        } 

        public void Update(User entity)
        {
            Remove(entity);
            Create(entity);
        }
    }
}