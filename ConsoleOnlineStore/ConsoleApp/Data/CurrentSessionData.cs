﻿using AutoMapper;
using BLL.Infrastructure;
using BLL.Services.Interfaces;
using BLL.Services.Realizations;
using DAL;
using DAL.Entities;
using DAL.Interfaces;
using DAL.Repositories.Interfaces;
using DAL.Repositories.Realizations;
using Ninject;

namespace ConsoleApp.Data
{
    public static class CurrentSessionData
    {
        public static ICategoryService Category { get; }
        public static IProductService Product { get; }
        public static IUserService User { get; }
        public static IOrderService Order { get; }
        
        public static User CurrentUser { get; set; }
        
        static CurrentSessionData()
        {
            var kernel = new StandardKernel();
            Register(kernel); 
            
            Category = kernel.Get<ICategoryService>();
            Product = kernel.Get<IProductService>();
            User = kernel.Get<IUserService>();
            Order = kernel.Get<IOrderService>();
        }
        
        private static void Register(IKernel kernel)
        {
            kernel.Bind<IProductRepository>().To<ProductRepository>();
            kernel.Bind<IUserRepository>().To<UserRepository>();
            kernel.Bind<IOrderRepository>().To<OrderRepository>();
            kernel.Bind<ICategoryRepository>().To<CategoryRepository>();
            
            kernel.Bind<IUoW>().To<UoW>();
            
            kernel.Bind<ICategoryService>().To<CategoryService>();
            kernel.Bind<IProductService>().To<ProductService>();
            kernel.Bind<IUserService>().To<UserService>();
            kernel.Bind<IOrderService>().To<OrderService>();

            var mapperConfiguration = new MapperConfiguration(cfg => { cfg.AddProfile<BllAutoMapperProfiles>(); });
            kernel.Bind<IMapper>().ToConstructor(c => new Mapper(mapperConfiguration)).InSingletonScope();
        }
    }
}