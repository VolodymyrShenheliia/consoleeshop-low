﻿using System;
using ConsoleApp.Data;

namespace ConsoleApp.UI
{
    public static class ProductUiComponents
    {
        public static void OutputList()
        {
            foreach (var product in CurrentSessionData.Product.GetAll())
            {
                Console.WriteLine($"{product.Name}  {product.Price}");       
            }

            Console.WriteLine();
            Console.Write("Press button to back in previous menu");
        }
    }
}