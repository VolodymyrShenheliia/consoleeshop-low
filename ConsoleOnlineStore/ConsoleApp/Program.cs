﻿using System;
using System.Linq;
using ConsoleApp.Data;
using ConsoleApp.UI;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Menu.StartMenu();
            
            Console.ReadLine();
        }
    }
}